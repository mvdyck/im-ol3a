function  [x , res , w , p , rho] = mycg(A,b,maxiter)
N = length(b) ;

% initialise the matrices that hold the vectors.
res  = zeros(N);
x  = zeros(N);
w  = zeros(N);
p = zeros(N);
 

rho=0*(1:maxiter);

% set the initial residu to the righthand side/
res(:,1)= b; % = b-Axo met xo=0
% calculate the initial rho
rho(1) = norm(res(:,1))^2;

for k=1:maxiter
    
     % the first search direction is in the direction of r_0
     if k-1 < 1
          p(:,k+1) = res(:,k);
     else
         % recurrence for the search directions
          beta = rho(k)/rho(k-1);
          p(:,k+1) = res(:,k) + beta*p(:,k);
     end
     w(:,k+1) = A*p(:,k+1);
     alpha = rho(k)/(p(:,k+1)'*w(:,k+1));
     x(:,k+1) = x(:,k) + alpha*p(:,k+1);
     res(:,k+1) = res(:,k) - alpha*w(:,k+1);
     rho(k+1) = norm(res(:,k+1))^2;
end
    
    
    
        